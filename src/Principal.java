import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Principal {

    //atributos
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static Empleado[] listaEmpleados = new Empleado[10];

    public static void main(String[] args) throws IOException {
         menu();
    }

    public static void menu() throws IOException {
        int opcion =0;
        do{
            System.out.println("*** Bievenido al Sistema ***");
            System.out.println("1. Registra Empleado.");
            System.out.println("2. Listar Empleados.");
            System.out.println("3. Salir.");
            System.out.print("Digite una opción: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while (opcion !=3);
    }

    public static void procesarOpcion(int opcion) throws IOException{
        switch (opcion)
        {
            case 1: registrarEmpleado();
                    break;
            case 2: listarEmpleados();
                    break;
            case 3:
                System.out.println("Gracias, vuelva pronto.");
                System.exit(0);
            default:
                System.out.println("No existe esa opción.");
        }
    }

    public static void registrarEmpleado() throws IOException{
        System.out.print("Ingrese la cedula: ");
        String cedula = in.readLine();
        System.out.print("Ingrese el nombre: ");
        String nombre = in.readLine();
        System.out.print("Ingrese el puesto: ");
        String puesto = in.readLine();

        Empleado empleado = new Empleado(cedula,nombre, puesto);

        for(int x =0;x < listaEmpleados.length;x++){
            if(listaEmpleados[x] == null){
                listaEmpleados[x] = empleado;
                x = listaEmpleados.length;
            }
        }
    }

    static public void listarEmpleados(){

        for (int i = 0; i < listaEmpleados.length; i++){
            if(listaEmpleados[i] != null) {
                System.out.println(listaEmpleados[i].toString());
            }
        }

    }


}









